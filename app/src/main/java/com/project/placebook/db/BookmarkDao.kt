package com.project.placebook.db

import androidx.lifecycle.LiveData
import androidx.room.*
import androidx.room.OnConflictStrategy.IGNORE
import androidx.room.OnConflictStrategy.REPLACE
import com.project.placebook.model.Bookmark

@Dao
interface BookmarkDao{
    //Live data keep user interface elements up to date when items change in the database
    //By default, Room won’t allow you to make calls to DAO methods on the main thread. By returning LiveData
    //objects, your method becomes an asynchronous query since live data is by default asynchronous
    @Query("SELECT * FROM Bookmark")
    fun loadAll(): LiveData<List<Bookmark>>
    @Query("SELECT * FROM Bookmark WHERE id = :bookmarkId")
    fun loadBookmark(bookmarkId: Long): Bookmark
    @Query("SELECT * FROM Bookmark WHERE id = :bookmarkId")
    fun loadLiveBookmark(bookmarkId: Long): LiveData<Bookmark>
    @Insert(onConflict = IGNORE)
    fun insertBookmark(bookmark: Bookmark): Long
    @Update(onConflict = REPLACE)
    fun updateBookmark(bookmark: Bookmark)
    @Delete
    fun deleteBookmark(bookmark: Bookmark)
}